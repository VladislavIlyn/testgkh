import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegusersComponent } from './views/regusers/regusers.component';
import { DefaultPageComponent } from './views/default-page/default-page.component';
import { LoginComponent } from './views/login/login.component';
import { DemotestComponent } from './views/demotest/demotest.component';
import { AskQuestionComponent } from './views/ask-question/ask-question.component';



const routes: Routes = [
  {
    path: '',
    component: DefaultPageComponent,
  }  
  ,
  {
    path: 'regusers',
    component: RegusersComponent,
    data: {
      title: 'Регистрация пользователей'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Вход в личный кабинет'
    }
  },
  {
    path: 'demotest',
    component: DemotestComponent,
    data: {
      title: 'Демо тест'
    }
  },
  {
    path: 'askquestion',
    component: AskQuestionComponent,
    data: {
      title: 'Задать вопрос'
    }
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
