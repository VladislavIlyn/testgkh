import { Component } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'testgkh2019';
  
  constructor(private router:Router) { }

  btn_login(){
    this.router.navigateByUrl('/login');
  }
}
