import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultPageComponent } from './views/default-page/default-page.component';
import { RegusersComponent } from './views/regusers/regusers.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './views/login/login.component';
import { DemotestComponent } from './views/demotest/demotest.component';
import { AskQuestionComponent } from './views/ask-question/ask-question.component';

@NgModule({
  declarations: [
    AppComponent,
    DefaultPageComponent,
    RegusersComponent,
    LoginComponent,
    DemotestComponent,
    AskQuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
