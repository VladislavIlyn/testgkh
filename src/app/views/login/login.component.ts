import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl,FormBuilder, FormGroup, Validators  } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  registerForm = new FormGroup({
    fc_login: new FormControl('', [Validators.required]),
    fc_password: new FormControl(),
  });

  submitted = false;
  is_sent=false;

  constructor(private router: Router,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fc_login: ['', [Validators.required]],
      fc_password: ['',[Validators.required]],
      phone:['',Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  btn_login()
  {
    this.submitted = true;
  }

  btn_reg(){
    this.router.navigateByUrl("/lkreg");
  }

  onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    //if (this.registerForm.invalid) {
    //    return;
    //}
    this.is_sent=true;
  }

}

