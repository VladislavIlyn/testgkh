import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormControl,ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';


@Component({
  selector: 'app-regusers',
  templateUrl: './regusers.component.html',
  styleUrls: ['./regusers.component.scss']
})
export class RegusersComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,private router: Router) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      surname:['',Validators.required],
      name:['',Validators.required],
      phone: ['', Validators.required],
      email:['',[Validators.required,Validators.email]],
      password: ['', [Validators.required,this.checkForLength]],
      confirmpassword: ['', Validators.compose([Validators.required])]
    },
    {
      validator: CustomValidators.passwordMatchValidator
    }
    );

  }

  get f() { return this.registerForm.controls; }

  onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
    this.router.navigateByUrl("");
  }

  checkForLength(control:FormControl){
      if(control.value.length<6){
        return{
          'lengthError':true
        };
      }
      return null;
  }

}

export class CustomValidators {
  static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }

      const valid = regex.test(control.value);

      return valid ? null : error;
    };
  }

  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('password').value; 
    const confirmpassword: string = control.get('confirmpassword').value; 
    if (password !== confirmpassword) {
      control.get('confirmpassword').setErrors({ NoPassswordMatch: true });
    }
  }
}
