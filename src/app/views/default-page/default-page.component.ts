import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-default-page',
  templateUrl: './default-page.component.html',
  styleUrls: ['./default-page.component.scss']
})
export class DefaultPageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  btn_reg(){
    this.router.navigateByUrl('/regusers');
  }

  btn_login(){
    this.router.navigateByUrl('/login');
  }

  btn_demo(){
    this.router.navigateByUrl('/demotest');
  }

}
